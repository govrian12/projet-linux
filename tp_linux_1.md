## Cr�ation utilisateur et ajout dans un groupe" ##

sudo adduser "username"
sudo passwd "username"
usermod -aG wheel "username"

visudo
Chercher la ligne :
"Allow root to run any commands anywhere"
Ajouter une ligne juste en dessous pour donner les droits root au nouveau compte :
"username" ALL=(ALL) ALL
 Enregistrer et quitter l��diteur :
:wq!
Le nouveau compte user peut maintenant ex�cuter des commandes en tant que root.

## Analyser le SSH ##

Afficher le statut du service SSH
systemctl status sshd
Active: active (running) since Mon 2021-12-13 12:11:13 CET; 58min ago

Commande ps aux 

admin       1749  0.0  0.2 235208  5284 pts/0    Ss   12:48   0:00 -bash
root        1839  0.0  0.0      0     0 ?        I    12:56   0:00 [kworker/1:3-events_power_efficient]
root        1852  0.0  0.0      0     0 ?        I    13:02   0:00 [kworker/1:0-events_power_efficient]
root        1853  0.0  0.0      0     0 ?        I    13:04   0:00 [kworker/0:0-events]
root        1856  0.0  0.0      0     0 ?        I    13:08   0:00 [kworker/1:1]
root        1857  0.0  0.0      0     0 ?        I    13:09   0:00 [kworker/0:1-ata_sff]
admin       1860  0.0  0.2 268528  3968 pts/0    R+   13:10   0:00 ps aux

## Modification du service SSH ##

vi /etc/ssh/sshd_config
Ajout de port "Nombre du port"

firewall-cmd --permanent --add-port=6666/tcp
firewall-cmd --reload

PS C:\Users\Govrian> ssh admin@192.168.110.100 -p 6666
admin@192.168.110.100's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Dec 13 12:48:30 2021 from 192.168.110.1
[admin@localhost ~]$

## Installer NGINX ##

sudo dnf upgrade
sudo dnf install nginx
sudo systemctl start nginx

sudo systemctl status nginx

? nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-13 13:31:54 CET; 6s ago
  Process: 37207 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 37205 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 37203 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 37208 (nginx)
    Tasks: 3 (limit: 11221)
   Memory: 5.0M
   CGroup: /system.slice/nginx.service
           +-37208 nginx: master process /usr/sbin/nginx
           +-37209 nginx: worker process
           +-37210 nginx: worker process

sudo systemctl enable nginx

[admin@localhost ~]$ sudo firewall-cmd --permanent --zone=public --add-service=http
success
[admin@localhost ~]$ sudo firewall-cmd --permanent --zone=public --add-service=https
success
[admin@localhost ~]$ sudo firewall-cmd --reload
success
[admin@localhost ~]$ sudo firewall-cmd --permanent --list-all
public
  target: default
  icmp-block-inversion: no
  interfaces:
  sources:
  services: cockpit dhcpv6-client http https ssh
  ports: 6666/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[admin@localhost ~]$

## Installer Python ##

sudo dnf install python39

[admin@localhost home]$ sudo firewall-cmd --permanent --add-port=8888/tcp
success
[admin@localhost home]$ sudo firewall-cmd --reload
success
[admin@localhost home]$ python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
Keyboard interrupt received, exiting.
[admin@localhost home]$ sudo firewall-cmd --reload
success
[admin@localhost home]$ python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
192.168.110.1 - - [13/Dec/2021 13:49:11] "GET / HTTP/1.1" 200 -
192.168.110.1 - - [13/Dec/2021 13:49:11] code 404, message File not found
192.168.110.1 - - [13/Dec/2021 13:49:11] "GET /favicon.ico HTTP/1.1" 404

## Cr�ation d'un service ##

/etc/systemd/system/, avec l'extension .service

[root@localhost home]# cat /etc/systemd/system/gov.service
[Unit]
Description=Test de service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

[root@localhost home]# systemctl daemon-reload
[root@localhost home]# sudo systemctl start gov.service
Failed to start gov.service: Unit gov.service has a bad unit file setting.
See system logs and 'systemctl status gov.service' for details.
[root@localhost home]# vi /etc/systemd/system/gov.service
[root@localhost home]# sudo systemctl start gov.service
[root@localhost home]# systemctl daemon-reload
[root@localhost home]# sudo systemctl start gov.service
[root@localhost home]# systemctl status gov.service
? gov.service - Test de service
   Loaded: loaded (/etc/systemd/system/gov.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-13 13:55:47 CET; 18s ago
 Main PID: 37814 (python3)
    Tasks: 1 (limit: 11221)
   Memory: 8.7M
   CGroup: /system.slice/gov.service
           +-37814 /usr/bin/python3 -m http.server 8888

Dec 13 13:55:47 localhost.localdomain systemd[1]: Started Test de service.
[root@localhost home]#

## Afiner la configuaration du service ##
## Cr�ation utilisateur Web et du dossier web ##

[root@localhost home]# cd
[root@localhost ~]# mkdir /srv/web/
[root@localhost ~]# cd /srv/
[root@localhost srv]# cd web/
[root@localhost web]# cd
[root@localhost ~]# ls
anaconda-ks.cfg
[root@localhost ~]# cd /s
sbin/ srv/  sys/
[root@localhost ~]# cd /srv/
[root@localhost srv]# ls
web
[root@localhost srv]# vi /srv/web/texte
[root@localhost srv]# cd web/
[root@localhost web]# ls
[root@localhost web]# ls
[root@localhost web]# vi /srv/web/texte
[root@localhost web]# ls
texte
[root@localhost web]# ls -m
texte
[root@localhost web]# ls -l
total 4
-rw-r--r--. 1 root root 5 Dec 13 14:01 texte
[root@localhost web]# sudo chown web texte
[root@localhost web]#
[root@localhost web]# ls -al
total 4
drwxr-xr-x. 2 root root 19 Dec 13 14:01 .
drwxr-xr-x. 3 root root 17 Dec 13 14:00 ..
-rw-r--r--. 1 web  root  5 Dec 13 14:01 texte
[root@localhost web]#


[root@localhost web]# cat /etc/systemd/system/gov.service
[Unit]
Description=Test de service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/web/

[Install]
WantedBy=multi-user.target

[root@localhost web]#

Cr�ation de clef Windows � mettre en in fichier dans .ssh avec authorized_keys

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDqNLdj0JxKcPoi0TiLhwOtyDTNpR5v3jydilIE/WN+GLYGGUbAQ7MI56IMmF0PzjCQLawQS3hJeeoyZo1KYv6EL62GK52MX/PTZfvXEdopueqg2ovLwDBGOw1YX8v9oN/JGnmv5Kpj1+kOOatKSxSKQd729gIho/4HnQhJWO72+p0u2N805/w2Ll2BWPIcZ8Bimdtw96EicltK7UCO4NYbHzbi73VyNnxgNIXZF9LZ3vkCZictV9m2Pc0FbUSXlrtgrbHDQ8loXUvnicYs+UBQFgdkrVYxRKKxeFIfUH36uvxR/uz60AQgtsc402JXjVo4A7TzPLmJND6M5lhJRKbkM3han7oMnYn+gGD7xU+9Xlr9yAZvsthF0v7QdkkCbOu8ng02Q3KEmLMar+w/hl1jU8HY/Cug1BdNNkdczdzqwt1kOmlSs/SiMHCvFGc4vJn7KnBphfNdsI5EIXe/caPFfi7nDNgnZoBVApdai4naGS85N6IAZRNWhX8k93X0U3mQeblLyTD5cDgkqI4MlXl6rQynAnDAEMun1IckC2hMB3fSpnkg9cQpt3XmxtlUt/xDbUeLxhL9zadRG6cp6zk8WOWVG+WDEFBFq2e+qU432jdWSqF2JbUrUtQyssgfGG+MKwLj4ViHadE1+XWomPrNnhLW9GqKYLNn5pqBk/P8Sw== govrian@LAPTOP-LHNIP2U8


[root@localhost .ssh]# chmod 600 authorized_keys
[root@localhost .ssh]# exit
exit
[admin@localhost home]$ exit
logout
Connection to 192.168.110.100 closed.
PS C:\Users\Govrian> ssh 192.168.110.100 -p 6666
govrian@192.168.110.100's password:
PS C:\Users\Govrian> ssh web@192.168.110.100 -p 6666
web@192.168.110.100's password:
Web console: https://localhost:9090/ or https://192.168.110.129:9090/

Last login: Mon Dec 13 14:22:07 2021 from 192.168.110.1
[web@localhost ~]$ exit
logout
Connection to 192.168.110.100 closed.
PS C:\Users\Govrian> ssh root@192.168.110.100 -p 6666
Web console: https://localhost:9090/ or https://192.168.110.129:9090/

Last login: Mon Dec 13 14:32:26 2021 from ::ffff:192.168.110.1
[root@localhost ~]# clear
[root@localhost ~]#

# Diasble home page NGINX # 

[root@localhost ~]# /etc/nginx/nginx.conf
-bash: /etc/nginx/nginx.conf: Permission denied
[root@localhost ~]# vi /etc/nginx/nginx.conf
[root@localhost ~]# cd /usr/share/nginx/
[root@localhost nginx]# ls
html  modules
[root@localhost nginx]# cd html/
[root@localhost html]# ls
404.html  50x.html  index.html  nginx-logo.png  poweredby.png
[root@localhost html]# mv index.html index.html.old
[root@localhost html]# systemctl reload nginx
[root@localhost html]#

# Mettre en place reverse proxy #

NON





