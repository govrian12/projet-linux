## Install MARIA DB ##

sudo yum install mariadb-server
sudo systemctl start mariadb
sudo systemctl enable mariadb
sudo systemctl status mariadb

? mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 13:32:50 CET; 13s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
 Main PID: 8745 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 11234)
   Memory: 83.8M
   CGroup: /system.slice/mariadb.service
           +-8745 /usr/libexec/mysqld --basedir=/usr

[root@localhost ~]# netstat -tlnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      941/sshd
tcp6       0      0 :::22                   :::*                    LISTEN      941/sshd
tcp6       0      0 :::3306                 :::*                    LISTEN      8745/mysqld

[root@localhost ~]# firewall-cmd --permanent --add-port=3306/tcp
success

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!

## Maria DB secure ##

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] n
 ... skipping.

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!

## Setup Apache ##

[root@db home]# dnf install epel-release
Last metadata expiration check: 2:43:28 ago on Tue 14 Dec 2021 11:10:10 AM CET.
Dependencies resolved.
======================================================================================================================================
 Package                            Architecture                 Version                           Repository                    Size
======================================================================================================================================
Installing:
 epel-release                       noarch                       8-13.el8                          extras                        23 k

Transaction Summary
======================================================================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
Downloading Packages:
epel-release-8-13.el8.noarch.rpm                                                                       12 kB/s |  23 kB     00:01
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                  10 kB/s |  23 kB     00:02
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : epel-release-8-13.el8.noarch                                                                                 1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                                                                 1/1
  Verifying        : epel-release-8-13.el8.noarch                                                                                 1/1

Installed:
  epel-release-8-13.el8.noarch

Complete!
[root@db home]#


[root@db home]# dnf update
Extra Packages for Enterprise Linux 8 - x86_64                                                        3.3 MB/s |  11 MB     00:03
Extra Packages for Enterprise Linux Modular 8 - x86_64                                                1.0 MB/s | 980 kB     00:00
Last metadata expiration check: 0:00:01 ago on Tue 14 Dec 2021 01:54:19 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                               Architecture               Version                            Repository                  Size
======================================================================================================================================
Upgrading:
 libsmbclient                          x86_64                     4.14.5-7.el8_5                     baseos                     147 k
 libwbclient                           x86_64                     4.14.5-7.el8_5                     baseos                     120 k
 samba-client-libs                     x86_64                     4.14.5-7.el8_5                     baseos                     5.4 M
 samba-common                          noarch                     4.14.5-7.el8_5                     baseos                     220 k
 samba-common-libs                     x86_64                     4.14.5-7.el8_5                     baseos                     172 k

Transaction Summary
======================================================================================================================================
Upgrade  5 Packages

Total download size: 6.0 M
Is this ok [y/N]: y
Downloading Packages:
(1/5): libwbclient-4.14.5-7.el8_5.x86_64.rpm                                                          693 kB/s | 120 kB     00:00
(2/5): libsmbclient-4.14.5-7.el8_5.x86_64.rpm                                                         598 kB/s | 147 kB     00:00
(3/5): samba-common-4.14.5-7.el8_5.noarch.rpm                                                         2.6 MB/s | 220 kB     00:00
(4/5): samba-common-libs-4.14.5-7.el8_5.x86_64.rpm                                                    1.9 MB/s | 172 kB     00:00
(5/5): samba-client-libs-4.14.5-7.el8_5.x86_64.rpm                                                    2.3 MB/s | 5.4 MB     00:02
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                 2.4 MB/s | 6.0 MB     00:02
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Running scriptlet: samba-common-4.14.5-7.el8_5.noarch                                                                          1/10
  Upgrading        : samba-common-4.14.5-7.el8_5.noarch                                                                          1/10
  Running scriptlet: samba-common-4.14.5-7.el8_5.noarch                                                                          1/10
  Upgrading        : samba-client-libs-4.14.5-7.el8_5.x86_64                                                                     2/10
  Upgrading        : libwbclient-4.14.5-7.el8_5.x86_64                                                                           3/10
  Upgrading        : samba-common-libs-4.14.5-7.el8_5.x86_64                                                                     4/10
  Upgrading        : libsmbclient-4.14.5-7.el8_5.x86_64                                                                          5/10
  Cleanup          : libsmbclient-4.14.5-2.el8.x86_64                                                                            6/10
  Cleanup          : samba-client-libs-4.14.5-2.el8.x86_64                                                                       7/10
  Cleanup          : samba-common-libs-4.14.5-2.el8.x86_64                                                                       8/10
  Running scriptlet: libwbclient-4.14.5-2.el8.x86_64                                                                             9/10
  Cleanup          : libwbclient-4.14.5-2.el8.x86_64                                                                             9/10
  Cleanup          : samba-common-4.14.5-2.el8.noarch                                                                           10/10
  Running scriptlet: libwbclient-4.14.5-7.el8_5.x86_64                                                                          10/10
  Running scriptlet: samba-common-4.14.5-2.el8.noarch                                                                           10/10
  Verifying        : libsmbclient-4.14.5-7.el8_5.x86_64                                                                          1/10
  Verifying        : libsmbclient-4.14.5-2.el8.x86_64                                                                            2/10
  Verifying        : libwbclient-4.14.5-7.el8_5.x86_64                                                                           3/10
  Verifying        : libwbclient-4.14.5-2.el8.x86_64                                                                             4/10
  Verifying        : samba-client-libs-4.14.5-7.el8_5.x86_64                                                                     5/10
  Verifying        : samba-client-libs-4.14.5-2.el8.x86_64                                                                       6/10
  Verifying        : samba-common-4.14.5-7.el8_5.noarch                                                                          7/10
  Verifying        : samba-common-4.14.5-2.el8.noarch                                                                            8/10
  Verifying        : samba-common-libs-4.14.5-7.el8_5.x86_64                                                                     9/10
  Verifying        : samba-common-libs-4.14.5-2.el8.x86_64                                                                      10/10

Upgraded:
  libsmbclient-4.14.5-7.el8_5.x86_64       libwbclient-4.14.5-7.el8_5.x86_64             samba-client-libs-4.14.5-7.el8_5.x86_64
  samba-common-4.14.5-7.el8_5.noarch       samba-common-libs-4.14.5-7.el8_5.x86_64

Complete!
[root@db home]# dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:00:21 ago on Tue 14 Dec 2021 01:54:19 PM CET.
remi-release-8.rpm                                                                                     12 kB/s |  26 kB     00:02
Dependencies resolved.
======================================================================================================================================
 Package                         Architecture              Version                              Repository                       Size
======================================================================================================================================
Installing:
 remi-release                    noarch                    8.5-2.el8.remi                       @commandline                     26 k

Transaction Summary
======================================================================================================================================
Install  1 Package

Total size: 26 k
Installed size: 21 k
Is this ok [y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : remi-release-8.5-2.el8.remi.noarch                                                                           1/1
  Verifying        : remi-release-8.5-2.el8.remi.noarch                                                                           1/1

Installed:
  remi-release-8.5-2.el8.remi.noarch

Complete!

[root@db home]# dnf module list php
Remi's Modular repository for Enterprise Linux 8 - x86_64                                             463  B/s | 858  B     00:01
Remi's Modular repository for Enterprise Linux 8 - x86_64                                             3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 8 - x86_64                                             1.3 MB/s | 947 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                            2.3 kB/s | 858  B     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                            3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                            2.9 MB/s | 2.0 MB     00:00
Rocky Linux 8 - AppStream
Name                   Stream                     Profiles                                     Summary
php                    7.2 [d]                    common [d], devel, minimal                   PHP scripting language
php                    7.3                        common [d], devel, minimal                   PHP scripting language
php                    7.4                        common [d], devel, minimal                   PHP scripting language

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name                   Stream                     Profiles                                     Summary
php                    remi-7.2                   common [d], devel, minimal                   PHP scripting language
php                    remi-7.3                   common [d], devel, minimal                   PHP scripting language
php                    remi-7.4                   common [d], devel, minimal                   PHP scripting language
php                    remi-8.0                   common [d], devel, minimal                   PHP scripting language
php                    remi-8.1                   common [d], devel, minimal                   PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled

## Module PHP ##

dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp

[root@db home]# systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service ? /usr/lib/systemd/system/httpd.service.
[root@db home]#

[root@db home]# systemctl status httpd
? httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/httpd.service.d
           +-php74-php-fpm.conf
   Active: active (running) since Tue 2021-12-14 13:59:29 CET; 8s ago
     Docs: man:httpd.service(8)
 Main PID: 10256 (httpd)
   Status: "Started, listening on: port 80"
    Tasks: 213 (limit: 11234)
   Memory: 35.1M
   CGroup: /system.slice/httpd.service
           +-10256 /usr/sbin/httpd -DFOREGROUND
           +-10265 /usr/sbin/httpd -DFOREGROUND
           +-10266 /usr/sbin/httpd -DFOREGROUND
           +-10267 /usr/sbin/httpd -DFOREGROUND
           +-10268 /usr/sbin/httpd -DFOREGROUND

Dec 14 13:59:18 db.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 14 13:59:29 db.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 14 13:59:34 db.tp2.cesi httpd[10256]: Server configured, listening on: port 80
[root@db home]#


[root@web ~]# dnf provides mysql
Last metadata expiration check: 3:28:22 ago on Tue 14 Dec 2021 11:10:10 AM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[root@web ~]#

## Connexion depuis web � db avec Mysql ##

Complete!
[root@web ~]# mysql -h 192.168.110.12 -u nextcloud -p nextcloud
Enter password:
ERROR 2003 (HY000): Can't connect to MySQL server on '192.168.110.12:3306' (113)
[root@web ~]#

[root@web ~]# mysql -h 192.168.244.12 -u nextcloud -p nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 28
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>



## Install APACHE ##

[root@web ~]# dnf install epel-release
Last metadata expiration check: 1:05:25 ago on Tue 14 Dec 2021 02:51:19 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                            Architecture                 Version                           Repository                    Size
======================================================================================================================================
Installing:
 epel-release                       noarch                       8-13.el8                          extras                        23 k

Transaction Summary
======================================================================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
Downloading Packages:
epel-release-8-13.el8.noarch.rpm                                                                       23 kB/s |  23 kB     00:00
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                  19 kB/s |  23 kB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : epel-release-8-13.el8.noarch                                                                                 1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                                                                 1/1
  Verifying        : epel-release-8-13.el8.noarch                                                                                 1/1

Installed:
  epel-release-8-13.el8.noarch

Complete!
[root@web ~]#

dnf update

dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm

dnf module list php

Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                              2.3 MB/s | 2.0 MB     00:00
Rocky Linux 8 - AppStream
Name                Stream                 Profiles                                 Summary
php                 7.2 [d]                common [d], devel, minimal               PHP scripting language
php                 7.3                    common [d], devel, minimal               PHP scripting language
php                 7.4                    common [d], devel, minimal               PHP scripting language

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name                Stream                 Profiles                                 Summary
php                 remi-7.2               common [d], devel, minimal               PHP scripting language
php                 remi-7.3               common [d], devel, minimal               PHP scripting language
php                 remi-7.4               common [d], devel, minimal               PHP scripting language
php                 remi-8.0               common [d], devel, minimal               PHP scripting language
php                 remi-8.1               common [d], devel, minimal               PHP scripting language


dnf module enable php:remi-7.4

## Analyser la conf Apache ##

